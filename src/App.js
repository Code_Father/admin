import React from 'react';
import Navbar from "./components/Navbar/Navbar";
import MainPage from "./components/MainPage/MainPage";
import {useState} from "react";


import Popup from "reactjs-popup";
import BurgerIcon from "./components/BurgerPopup/BurgerIcon";
import Menu from "./components/BurgerPopup/Menu";
import "./components/BurgerPopup/index.css";

import './App.css';
import "./media.css";

const styles = {
    fontFamily: "sans-serif",
    textAlign: "center",
    marginTop: "40px"
};
const contentStyle = {
    background: "rgba(255,255,255,0",
    width: "80%",
    border: "none"
};


function App() {
    const [isOpen,setIsOpen] = useState(false);


    const setActive = () => {
        setIsOpen(!isOpen);
    };


    let asideMenu = isOpen ? <Navbar /> : null;

  return (
    <div className="App">
        {/*<Navbar />*/}
        {asideMenu}
        <MainPage toggle={setActive}  />
        <Popup
            modal
            overlayStyle={{ background: "#19313d" }}
            contentStyle={contentStyle}
            closeOnDocumentClick={false}
            trigger={open => <BurgerIcon open={open} />}
        >
            {close => <Menu close={close} />}
        </Popup>
    {/*  Main Page-props  toggle={setActive}*/}
    </div>
  );
}

export default App;
