import React from "react";
import logo from "../../img/logo.png";
import chat from "../../img/chat.png";
import documents from "../../img/documents.png";
import email from "../../img/email.png";
import events from "../../img/events.png";
import extra from "../../img/extra.png";
import groups from "../../img/groups.png";
import home from "../../img/home.png";
import people from "../../img/people.png";
import training from "../../img/training.png";export default ({close}) => (
    <div className="menu">
        <ul>
            <li><img src={logo} alt="logo"/></li>
            <li className="drop-down-menu-link" onClick={close}><div><img src={home} alt="home"/></div> <p>Home</p></li>
            <li className="drop-down-menu-link"  onClick={close}><div><img src={chat} alt="chat"/></div><p>Chat</p></li>
            <li className="drop-down-menu-link"  onClick={close}><div><img src={groups} alt="groups"/></div><p>Groups</p></li>
            <li className="drop-down-menu-link"  onClick={close}><div><img src={training} alt="training"/></div><p>Training</p></li>
            <li  className="drop-down-menu-link" onClick={close}><div><img src={events} alt="events"/></div><p>Events</p></li>
            <li className="drop-down-menu-link"  onClick={close}><div><img src={documents} alt="documents"/></div><p>Documents</p></li>
            <li className="drop-down-menu-link"  onClick={close}><div><img src={people} alt="people"/></div><p>People</p></li>
            <li className="drop-down-menu-link"  onClick={close}><div><img src={email} alt="email"/></div><p>Email</p></li>
            <li className="drop-down-menu-link"  onClick={close}><div><img src={extra} alt="extra"/></div><p>Extra</p></li>
        </ul>
    </div>
);
