import React, {Component} from 'react'
import {Line} from 'react-chartjs-2'

class LineChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 'line',
            data: {
                labels: [
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    6,
                    7,
                    8,
                    9,
                    10
                ],
                datasets: [
                    // {lineTension:0},
                    {
                         label: 'Series 1',//  Name the series
                        data: [
                            7,
                            5.9,
                            6.2,
                            4.9,
                            5,
                            3.8,
                            2.9,
                            4,
                            2.9,
                            3,
                            1.1,
                            2], // Specify the data values array
                        fill: false,
                        borderColor: '#E74C3C', // Add custom color border (Line)
                        backgroundColor: '#E74C3C', // Add custom color background (Points and Fill)
                        borderWidth: 1, // Specify bar border width
                        lineTension: 0
                    }, {   label: 'Series 2', // Name the series
                        data: [
                            1,
                            2.8,
                            2.1,
                            3,
                            3.3,
                            5.2,
                            4.8,
                            7.1,
                            6.1,
                            5.9,
                            7.1,
                            5,
                            2], // Specify the data values array
                        fill: false,
                        borderColor: '#3094D6', // Add custom color border (Line)
                        backgroundColor: '#3094D6', // Add custom color background (Points and Fill)
                        borderWidth: 1, // Specify bar border width
                        lineTension: 0
                    }
                ]
            },
            options: {
                responsive: true, // Instruct chart js to respond nicely.
                animationEnabled: true
            }
        }
        ;
    }

    render() {
        return (
            <div className="line-chart">
                <div id="chart">

                    <Line data={this.state.data} options={this.state.options} type="area"/>
                </div>
            </div>
        )
    }
}

export default LineChart;