import React, {Component} from 'react';
import "./menuItem.css";

class MenuItem extends Component {
    render() {
        return (
            <div className={"menu-single " + this.props.status}>
                <img src={this.props.image} alt={this.props.info}/>

                <p>{this.props.info}</p>
            </div>
        );
    }
}

export default MenuItem;