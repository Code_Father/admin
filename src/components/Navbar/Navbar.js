import React, {Component} from 'react';
import logo from "../../img/logo.png";
import chat from "../../img/chat.png";
import documents from "../../img/documents.png";
import email from "../../img/email.png";
import events from "../../img/events.png";
import extra from "../../img/extra.png";
import groups from "../../img/groups.png";
import home from "../../img/home.png";
import people from "../../img/people.png";
import training from "../../img/training.png";
import "./navbar.css";
import MenuItem from "../MenuItem/MenuItem";

class Navbar extends Component {
    constructor() {
        super();
        this.state = {
            images: [{
                info: "Home",
                img: home,
                status: "active"
            }, {
                info: "Chat",
                img: chat,
                status: ""
            }, {
                info: "Groups",
                img: groups,
                status: ""
            }, {
                info: "Training",
                img: training,
                status: ""
            },
                {
                    info: "Events",
                    img: events,
                    status: ""
                },
                {
                    info: "Documents",
                    img: documents,
                    status: ""
                },
                {
                    info: "People",
                    img: people,
                    status: ""
                },
                {
                    info: "Email",
                    img: email,
                    status: ""
                },
                {
                    info: "Extra",
                    img: extra,
                    status: ""
                },
            ]
        }
    }

    render() {
        return (
            <div>
                <div className="nav-bar-left">
                    <img src={logo} alt="logo" className="logo"/>
                    {
                        this.state.images.map((item) => {

                            return (
                                <div key={item.info}>
                                    <div className="three-dots">
                                        <div className="dot-single"></div>
                                        <div className="dot-single"></div>
                                        <div className="dot-single"></div>
                                    </div>
                                    <MenuItem key={item.info} status={item.status} image={item.img} info={item.info}/>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        );
    }
}

export default Navbar;