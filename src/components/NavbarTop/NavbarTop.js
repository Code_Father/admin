import React, {Component} from 'react';
import "./NavbarTop.css";
import avatar from "../../img/avatar.png";
import search from "../../img/search.png";
import plus from "../../img/plus.png";
import todo from "../../img/to-do-list.png";
import notification from "../../img/notification.png";

class NavbarTop extends Component {
    render() {
        return (
            <div className="nav-bar-top">
                <div className="nav-bar-first-container">
                    <div onClick={this.props.toggle} className="burger-menu-big-size">
                        {/*onClick={this.props.toggle}*/}
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <div className="search-container">
                        <img src={search} alt="search"/>
                        <input type="text" className="search" placeholder="Search..."/>
                    </div>
                </div>
                <div className="nav-bar-second-container">
                    <div className="drop-down-container">
                        <img src={plus} alt="plusIcon"/>
                        <select name="" id="" className="drop-down">
                            <option value=""  defaultChecked>Create</option>
                        </select>
                    </div>
                    <div className="icon-container icon">
                        <img src={todo} alt="to-do"/>
                        <div className="icon-element">
                            3
                        </div>
                    </div>
                    <div className="icon-container icon">
                        <img src={notification} alt="notification"/>
                        <div className="icon-element">
                            3
                        </div>
                    </div>

                    <div className="icon-container avatar">
                        <img src={avatar} alt="avatar"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default NavbarTop;