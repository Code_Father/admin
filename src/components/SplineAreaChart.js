import React, {Component} from 'react'
import {  Line} from 'react-chartjs-2'

class SplineAreaChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 'line',
            data: {
            },
            options: {
                    legend: {
                        display: false
                    },
                    tooltips: {
                        enabled: false
                    },
                responsive: true, // Instruct chart js to respond nicely.
                //   maintainAspectRatio: false,  Add to prevent default behaviour of full-width/height
            }
        };
    }





    render() {
        return (
            <div id="chart">

                <Line data={this.props.data} type="area"/>
            </div>
        )
    }
}
export default SplineAreaChart;